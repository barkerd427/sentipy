# CS 410 project

# Setup

Developed and tested with Python 3.x

For Aaron specifically:

```
pyenv shell 3.6.7
```

For dependencies

```
pip install -r requirements.txt 
python -m textblob.download_corpora
```

Update the twitter credentials.

```
cp config/example_credentials.sh config/credentials.sh
source config/credentials.sh
```

Run the gathering of data:

```
./get_tweets.py
./get_sentiment.py
./build_map.py
```

There is a possibility this will cause timeouts and/or hit rate limits, so you may want to run for one company at a time.

```
./get_tweets.py -c delta
./get_sentiment.py -c delta
./build_map.py -c delta
```

Since 100 is only twice as many as the states, you will likely want more than just the default 100 tweets.

```
./get_tweets.py -c SouthwestAir -n 200
./get_sentiment.py -c SouthwestAir
./build_map.py -c SouthwestAir
```

NOTE: you will get an error like:
```
json.decoder.JSONDecodeError: Extra data: line 12627 column 2 (char 482649)
```

You will need to remove the line that is like `][` and add a comma.

```
./get_tweets.py -c united -n 300
./get_sentiment.py -c united
./build_map.py -c united
```

To start the Django server you would run:

```
python manage.py runserver
```

NOTE: given the way that the html is written currently. It is not rendering the embedded links in Django, so we simply used Django to write the static HTML.

This can be found in `multi-map.html`

# End Result

All of this is deployed to a static site using Gitlab Pages.

This can be found at: https://barkerd427.gitlab.io/sentipy/

# Data cleansing

* Make lowercase
* Removed special characters
* Removed links
* Removed short words
* Removed extra spaces
* Only used tweets where we could find a location
* The location was determined in this order
  * coordinates of the tweet
  * geo-location of the tweet
  * place of the tweet
  * user location (from profile) - NOTE: many of these were non-sensical
* Only used tweets with US based locations

# Reading the Map

Polarity is shown on scale of Purple, Blue, Green.

* Purple means: negative sentiment
* Blue means: neutral sentiment
* Green means: positive sentiment
* Grey means: no data present

# Interesting observations

Some airlines do not service some states, so it makes sense to not see data in those areas.

Twitter is a common platform for complaints. We were pleased to see the amount of green that we saw.
