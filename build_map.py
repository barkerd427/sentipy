#!/usr/bin/env python
import os
import pandas as pd
import numpy as np
import folium
import json
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-c", "--company", dest="company",
                  help="company")

(options, args) = parser.parse_args()
print("company = {0}".format(options.company))

state_sentiment = "data/sentiment.csv"
if options.company:
    state_sentiment = "data/sentiment_" + options.company + ".csv"
df = pd.read_csv(state_sentiment)
#print(df.head())
output = pd.pivot_table(df,index=["state"])
if 'None' in open(state_sentiment).read():
    output.drop('None',inplace=True)
if 'non-US' in open(state_sentiment).read():
    output.drop('non-US',inplace=True)
print(output)
output.to_csv(state_sentiment)

#From: https://python-graph-gallery.com/292-choropleth-map-with-folium/
state_geo = os.path.join('sentipy/static/', 'us-states.json')

#state_sentiment = os.path.join('data/', 'state_sentiment.csv')
state_data = pd.read_csv(state_sentiment)
m = folium.Map(location=[37, -102], zoom_start=3)

# FutureWarning: The choropleth  method has been deprecated. Instead use the new Choropleth class, which has the same arguments. See the example notebook 'GeoJSON_and_choropleth' for how to do this.
# See: https://github.com/python-visualization/folium/blob/master/examples/GeoJSON_and_choropleth.ipynb
folium.Choropleth(
 geo_data=state_geo,
 name='choropleth',
 data=state_data,
 columns=['state', 'polarity'],
 key_on='feature.id',
 # http://python-visualization.github.io/folium/docs-v0.5.0/modules.html
 fill_color='PuBuGn',
 fill_opacity=0.7,
 line_opacity=0.2,
 zoom_start=2,
 max_zoom=10,
 height='60%',
 width='60%',
 legend_name='Sentiment Polarity (%)'
).add_to(m)

# Save to html
fName = "data/sentiment.html"
if options.company:
    fName = "data/sentiment_" + options.company + ".html"
m.save(fName)