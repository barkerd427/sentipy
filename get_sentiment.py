#!/usr/bin/env python

import tweepy
import json
import jsonpickle
import os
import pandas as pd
import numpy as np
import io
import re
from optparse import OptionParser
from textblob.sentiments import NaiveBayesAnalyzer
from tweepy import OAuthHandler
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
from textblob import Blobber

us_state_abbrev = {
    'Alabama': 'AL',
    'Alaska': 'AK',
    'Arizona': 'AZ',
    'Arkansas': 'AR',
    'California': 'CA',
    'Colorado': 'CO',
    'Connecticut': 'CT',
    'Delaware': 'DE',
    'Florida': 'FL',
    'Georgia': 'GA',
    'Hawaii': 'HI',
    'Idaho': 'ID',
    'Illinois': 'IL',
    'Indiana': 'IN',
    'Iowa': 'IA',
    'Kansas': 'KS',
    'Kentucky': 'KY',
    'Louisiana': 'LA',
    'Maine': 'ME',
    'Maryland': 'MD',
    'Massachusetts': 'MA',
    'Michigan': 'MI',
    'Minnesota': 'MN',
    'Mississippi': 'MS',
    'Missouri': 'MO',
    'Montana': 'MT',
    'Nebraska': 'NE',
    'Nevada': 'NV',
    'New Hampshire': 'NH',
    'New Jersey': 'NJ',
    'New Mexico': 'NM',
    'New York': 'NY',
    'North Carolina': 'NC',
    'North Dakota': 'ND',
    'Ohio': 'OH',
    'Oklahoma': 'OK',
    'Oregon': 'OR',
    'Pennsylvania': 'PA',
    'Rhode Island': 'RI',
    'South Carolina': 'SC',
    'South Dakota': 'SD',
    'Tennessee': 'TN',
    'Texas': 'TX',
    'Utah': 'UT',
    'Vermont': 'VT',
    'Virginia': 'VA',
    'Washington': 'WA',
    'West Virginia': 'WV',
    'Wisconsin': 'WI',
    'Wyoming': 'WY',
}

parser = OptionParser()
parser.add_option("-c", "--company", dest="company",
                  help="company")

(options, args) = parser.parse_args()
print("company = {0}".format(options.company))

blobber = Blobber(analyzer=NaiveBayesAnalyzer())
geolocator = Nominatim(user_agent="sentipy")

def clean_tweet(tweet): 
        ''' 
        Utility function to clean tweet text by removing links, special characters 
        using simple regex statements. 
        '''
        #unicode(tweet, 'ascii', 'ignore')
        return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split())

def get_tweet_sentiment(tweet): 
        ''' 
        Utility function to classify sentiment of passed tweet 
        using textblob's sentiment method 
        '''
        # create TextBlob object of passed tweet text 
        analysis = blobber(clean_tweet(tweet))
        # set sentiment 
        # if analysis.sentiment.polarity > 0: 
        #     return 'positive'
        # elif analysis.sentiment.polarity == 0: 
        #     return 'neutral'
        # else: 
        #     return 'negative'
        if (analysis.sentiment.p_pos > 0.6): 
            return 'positive'
        elif (analysis.sentiment.p_pos < 0.6 and analysis.sentiment.p_pos > 0.4): 
            return 'neutral'
        else: 
            return 'negative'

def get_tweet_polarity(tweet): 
        analysis = blobber(clean_tweet(tweet)) 
        print("polarity = {0}".format(analysis.sentiment.p_pos))
        return analysis.sentiment.p_pos

def get_state(coordinates):
    location = None
    try:
        location = geolocator.reverse(coordinates, timeout=None)
    except GeocoderTimedOut as e:
        print("Error: geocode failed on input %s with message %s"%(coordinates, e))
    except:
        print("Exception occurred... skipping")
    if (location != None):
        try:
            #print(type(location))
            print(location)
        except TypeError:
            abbreviation = ''
            return abbreviation
        if location.address:
            if('state' in location.raw['address']):
                state = location.raw['address']['state']
                #print("state = {0}".format(state))
                abbreviation = us_state_abbrev.get(state, "non-US")
                print("abbreviation = {0}".format(abbreviation))
            else:
                abbreviation = ''
        else:
            abbreviation = '' 
    else:
        abbreviation = ''
    return abbreviation 

fName = "data/tweets.json"
if options.company:
    fName = "data/tweets_" + options.company + ".json"

tweet_list = []
coordinate_list = []
sentiment_location_list = []
state = None
with io.open(fName, encoding='utf-8') as json_file:  
    all_data = json.load(json_file)
    for each_dictionary in all_data:
        tweet_id = each_dictionary['id']
        text = each_dictionary['full_text']
        sentiment = get_tweet_sentiment(text)
        polarity = get_tweet_polarity(text)
        favorite_count = each_dictionary['favorite_count']
        retweet_count = each_dictionary['retweet_count']
        created_at = each_dictionary['created_at']
        coordinates = each_dictionary['coordinates']
        user_location = each_dictionary['user']['location']
        geo = each_dictionary['geo']
        place = each_dictionary['place']
        if (coordinates != None):
            coordinates = coordinates['coordinates'].reverse()
            state = get_state(coordinates)
        elif (geo != None):
            coordinates = geo['coordinates']
            state = get_state(coordinates)
        elif (place != None):
            coordinates = place['bounding_box']['coordinates'][0][0]
            rev_coordinates = coordinates[::-1]
            state = get_state(rev_coordinates)
        elif (user_location != ''):
            location = None
            try:
              location = geolocator.geocode(user_location, timeout=None)
            except GeocoderTimedOut as e:
                print("Error: geocode failed on input %s with message %s"%(user_location, e))
            except:
                print("Exception occurred")
            if (location == None):
                coordinates = None
                state = None
            else:
                coordinates = [location.latitude, location.longitude]
                state = get_state(coordinates)
        else:
            coordinates = None
            state = None

        tweet_list.append({'tweet_id': str(tweet_id),
                             'text': text.encode('utf-8'),
                             'sentiment': str(sentiment),
                             'polarity': polarity,
                             'favorite_count': int(favorite_count),
                             'retweet_count': int(retweet_count),
                             'created_at': created_at,
                             'coordinates': coordinates,
                             'state': str(state),
                            })

        tweet_json = pd.DataFrame(tweet_list, columns = 
                                  ['tweet_id', 'text', 'sentiment',
                                  'polarity',
                                   'favorite_count', 'retweet_count', 
                                   'created_at', 'coordinates',
                                   'state'
                                   ])
        coordinate_list.append(coordinates)
        sentiment_location_list.append({'state': str(state),
                                        'polarity': polarity,
                                        })
        sentiment_csv = pd.DataFrame(sentiment_location_list, columns = 
                                    [ 'state', 'polarity'
                                    ])

tweet_fName = "data/tweet_data.csv"
if options.company:
    tweet_fName = "data/tweet_data_" + options.company + ".csv"
tweet_json.to_csv(path_or_buf=tweet_fName)

print(tweet_json['sentiment'].value_counts())

sent_fName = "data/sentiment.csv"
if options.company:
    sent_fName = "data/sentiment_" + options.company + ".csv"
sentiment_csv.set_index('state').to_csv(sent_fName)
