from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse
import csv

# Create your views here.
class HomePageView(TemplateView):
    def get(self, request, **kwargs):
        args = {}
        with open("airlinelist.csv") as f:
            pre_reader = csv.reader(f)
            column = {}
            headers = next(pre_reader, None)
            for h in headers:
                column[h] = []
            for row in pre_reader:
                for h, v in zip(headers, row):
                    column[h].append(v)
            args['column'] = column
            args['twitter_list'] = column['Twitter']
            args['list'] = pre_reader
            companies_dict = {rows[0]:rows[1] for rows in pre_reader}
            args['companies_dict'] = companies_dict
            twitter_dict = {rows[0]:rows[2] for rows in pre_reader}
            args['twitter_dict'] = twitter_dict            
            #for i, line in enumerate(reader):
            #    print 'line[{}] = {}'.format(i, line)
            return render(request, 'index.html', args)

# Add this view
#class AboutPageView(TemplateView):
#    template_name = "about.html"
